// Helpers can be easily unit tested and reused
const isValidNumber = (value) => typeof value === 'number' && !isNaN(value);

module.exports = { isValidNumber };
  