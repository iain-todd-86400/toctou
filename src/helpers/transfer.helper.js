// Helpers can be easily unit tested and reused
const canTransfer = (amount, balance) => amount && amount > 0 && amount <= balance;

module.exports = { canTransfer };
  