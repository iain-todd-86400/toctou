const { isValidNumber } = require('../helpers/number.helper');

class Amount {
    value = null;

    constructor(value) {
        let _value = Number(value);

        if (value !== null && isValidNumber(_value)) {
            this.value = _value;
            Object.freeze(this);
            return;
        }

        throw new TypeError('Invalid value');        
    }
}

module.exports = Amount;